# frozen_string_literal: true

require File.expand_path '../spec_helper.rb', __FILE__

RSpec.describe CoffeeShopList do
  describe 'With valid params' do
    it 'return an array of hash' do
      service = CoffeeShopList.new('N80LJ')
      expect(service.call.is_a?(Array)).to be_truthy
      expect(service.call.first.is_a?(Hash)).to be_truthy
    end

    describe 'Attributes' do
      it 'has name, address, rating as attributes' do
        coffee_shop = CoffeeShopList.new('N80LJ').call.first
        expect(coffee_shop.keys).to eq %i[name address rating]
      end
    end

    describe 'Rating' do
      it 'return coffee shops sorted by rating' do
        items = CoffeeShopList.new('N80LJ').call
        item1_rating = items.first[:rating]
        item2_rating = items.last[:rating]
        expect(item1_rating > item2_rating).to be_truthy
      end
    end
  end
end
