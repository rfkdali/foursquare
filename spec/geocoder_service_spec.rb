# frozen_string_literal: true

require File.expand_path '../spec_helper.rb', __FILE__

RSpec.describe GeocoderService do
  describe 'With valid postcode' do
    it 'returns coordinates' do
      coordinates = GeocoderService.new('N80LJ').call
      expect(coordinates).to eq '51.5926449, -0.1079937'
    end

    it 'returns coordinates (spaced postcode)' do
      coordinates = GeocoderService.new('N8 0LJ').call
      expect(coordinates).to eq '51.5926449, -0.1079937'
    end
  end
  describe 'With invalid postcode' do
    describe 'With empty value' do
      it 'returns an error' do
        service = GeocoderService.new('')
        expect { service.call }.to raise_error(ArgumentError)
      end
    end

    describe 'With an unknown postcode' do
      it 'returns an error - unknown postcode' do
        service = GeocoderService.new('QPORFDSE')
        expect { service.call }.to raise_error(ArgumentError)
      end
    end
  end
end
