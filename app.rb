# frozen_string_literal: true

require 'sinatra'
require 'haml'

require_relative 'lib/coffee_shop_list'
require_relative 'lib/geocoder_service'
require_relative 'lib/foursquare_service'

set :show_exceptions, :after_handler

error ArgumentError do
  redirect to('/postcode_error')
end

get '/' do
  haml :index, locals: { error: nil }
end

post '/foursquare' do
  valid_params = params['postcode'].gsub(' ', '%20')
  redirect "/coffee_shop_list/#{valid_params}"
end

get '/coffee_shop_list/:param' do |postcode|
  @coffee_shop_list = CoffeeShopList.new(postcode).call
  haml :index, locals: { coffee_shop_list: @coffee_shop_list, error: nil }
end

get '/postcode_error' do
  error = 'Please retry with a valid postcode'
  haml :index, locals: { coffee_shop_list: nil, error: error }
end
