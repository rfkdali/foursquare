# frozen_string_literal: true

require 'geocoder'

# Use Geocoder to get coordinates from a postcode
class GeocoderService
  def initialize(postcode)
    @postcode = postcode
  end

  def call
    result = Geocoder.coordinates(@postcode)
    raise ArgumentError if result.nil?
    result.join(', ')
  end
end
