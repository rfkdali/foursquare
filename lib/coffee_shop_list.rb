# frozen_string_literal: true

# Get a list of coffee shops, sorted by rating
class CoffeeShopList
  def initialize(postcode)
    @postcode = postcode
  end

  def call
    formatted_coffee_shops unless coordinates.nil?
  end

  private

  def coordinates
    GeocoderService.new(@postcode).call
  end

  def popular_coffees
    response = FoursquareService.new(coordinates).call

    response.sort_by do |item|
      item['venue']['rating'].to_s
    end
  end

  def formatted_coffee_shops
    coffee_shops = []

    popular_coffees.each do |item|
      coffee_shops << {
        name: item['venue']['name'],
        address: item['venue']['location'],
        rating: item['venue']['rating'].to_s
      }
    end
    coffee_shops.reverse
  end
end
