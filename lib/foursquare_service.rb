# frozen_string_literal: true

require 'httparty'

# Call Foursquare to get a list of coffeeshop
class FoursquareService
  include HTTParty

  base_uri 'https://api.foursquare.com'

  def initialize(latitude_longitude)
    @options = {
      query: {
        ll: latitude_longitude,
        categoryId: '4bf58dd8d48988d1e0931735', # COFFEESHOPS CATEGORY ID
        client_id: 'WAMOYWKSVBLDXMXLEONCZ0NYUWPMOI3GF3X22LF5KOVXYLKX',
        client_secret: 'WAIMERRVPCIB5HMPAKSC0K2TN5IPK1NR1OOUZCIT35V4PRIZ',
        v: '20170501'
      }
    }
  end

  def call
    response = self.class.get('/v2/venues/explore', @options)
    response.parsed_response['response']['groups'][0]['items']
  end
end
